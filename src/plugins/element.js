import Vue from 'vue'
import {
  Button,
  Col,
  Row,
  Drawer,
  Card,
  Menu,
  MenuItem,
  Container,
  Header,
  Main,
  Badge,
} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

locale.use(lang)

Vue.use(Button)
Vue.use(Col);
Vue.use(Row);
Vue.use(Drawer);
Vue.use(Card);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(Container);
Vue.use(Header);
Vue.use(Main);
Vue.use(Badge);
