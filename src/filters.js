import Vue from 'vue'

Vue.filter('toPrice', function (value) {
  if (typeof value !== "number") {
    return value;
  }
  var formatter = new Intl.NumberFormat('it-IT', {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 0
  });
  return formatter.format(value);
})
